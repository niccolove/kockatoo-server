## Attention, this project is currently in alpha stage , use at your own risk

# Kockatoo Server

## About
Kockatoo lets you upload posts to various social media platforms at just one click. 
The following platforms are supported right now -:
* Twitter 
* Mastodon 
* Reddit

This is the server side code for the same 

## How to build and run 

* Create a virtualenv by using mkvirtualenv or whatever way you prefer 
* Run ```python -m pip install -r requirements.txt```
### Start initiliazing the API keys
 
 * Create a directory called creds in platforms

#### Initiliaze mastodon
* Run ```python platforms/mastodon_post/mastodon_init.py```

#### Initiliaze twitter
* Head to https://developer.twitter.com/en/apply-for-access
* Now click on Apply for a developer account 
* Now you follow the intructions 
* Now once you have filled the application and verified your email again click on create new project and follow the instructions
* Now you will be lead to a dashboard where you will see your project name and the name of the app
* In the line where there is the name of the app you will see a key , Click on that 
* Now for consumer keys click on regenerate and save these
* Do the same for access token and secret 
* Run ```python platforms/twitter/twitter_init.py```

#### Initiliaze reddit 
* Login to reddit 
* Now head to https://www.reddit.com/prefs/apps
* Click on create an app 
    - Add a name
    - select script as the type 
    - Enter a description
    - in redirect url enter https://localhost:8080
    - Now click on create app
* Now create a file called client_secrets.json in platforms/creds/ which has contents similar to this
```json 
{
    "client_id":"",
    "client_secret":"",
    "user_agent":"",
    "redirect_uri":"",
    "refresh_token":" 
}

```
* Add all the details as follows 
    - client_id  - just under the personal use script
    - client_secret - next to the word secret
    - user_agent - whatever name of the app you want
    - redirect_uri - copy from the redirect URI you had entered 
    - refresh token - you are going to obtain this in a second

* Run ```python platforms/reddit/reddit_post.py```

#### Start the server
* Now run ```uvicorn main:app --reload```

## Basic API usage 
#### Basic usage of the API is as follows 
## Schedule as many posts as you like 
* localhost:8000/post/twitter?text=Never gonna give you up&image=./test.png&time=2021-03-14 10:40:00.0088 
* localhost:8000/post/mastodon?text=Never gonna give you up&image=./test.png&time=2021-03-14 10:40:00.00
* localhost:8000/post/all?text=Never gonna give you up&time=2021-03-14 10:40:00.00

## After you are done adding the posts start the scheduler
* localhost:8000/sched/start

#### Another good way to test out the API could be via the ui docs

* Go to localhost:8000/docs
* Click on the blue line which says GET and you will see a dropdown 
* Now click on 'Try it out' on the top right
    * In platform you can input twitter, mastodon, reddit or all 
    * In image you have to put the path to whatever image you want to post 
    * Here are is an example for time 
        - 2021-03-14 10:40:00.00
        - If you dont add a time the default is two minutes after the post has been scheduled
        - So the format is kinda like yyyy-mm-dd HH:MM:SS.SS
    * text is whatever text you want to go with the image (not the caption)
    * subtext is for reddit which is basically heading of the reddit post
    * subr is the subreddit for reddit where you want to post
    * mastodon_base_url is the base url of the mastodon instance you are authenticating with 
* Now click on execute and schedule the post
* Now scroll down the sched tag and find the sched/{action} endpoint there, To actually post you need to start the scheduler so click on try it out and then under action type start and click on execute .

#### If you want to see all the endpoints  
* localhost:8000/docs
* localhost:8000/redoc

## Features Yet to be implemented to get this project upto KDE standards
* Incorporate API keys to manage authentication
* Do some more exception handling

## Bug reporting 
* Please report any bugs you find in the issues section 
