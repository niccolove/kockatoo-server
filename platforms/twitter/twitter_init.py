import tweepy
from json import dump
import pickle
from pathlib import Path
import os 

creds_path = os.environ['CREDS_PATH'] + '/twitter.pickle'

consumer_key =  input("Enter the twitter consumer key: ")
consumer_secret = input("Enter the consumer secret: ")
access_token = input("Enter the Access token: ")
access_token_secret = input("Enter the access token secret: ")

twitter_auth_keys = { 
        "consumer_key"        : consumer_key,
        "consumer_secret"     : consumer_secret,
        "access_token"        : access_token,
        "access_token_secret" : access_token_secret
}
with open(creds_path, 'wb') as outfile:
    pickle.dump(twitter_auth_keys, outfile)