import pickle 
from pathlib import Path
import os 

user_path = os.environ['CREDS_PATH'] + '/users.pickle'

SECRET_KEY = "023687880321daf701dfff4ea6e83635001179e675ffc044b2ed58cfb8bf9571"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 10080

salt = '241ae2a0c2a11d8731fabf6ce43ee91a65bd2801c2876be9feebdd15c8ee83e0'
master_key = '12f0c344b3ddc9e8e5a783d5677e1dcc7b0f7925b83341409e77ea1402856136'

if os.path.getsize(user_path) > 0:      
    with open(user_path,'rb') as f:
        users = pickle.load(f)
else:
    users = {}